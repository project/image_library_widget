<?php

declare(strict_types = 1);

namespace Drupal\Tests\image_library_widget\FunctionalJavascript;

use Drupal\Core\Entity\Entity\EntityFormDisplay;
use Drupal\entity_test\Entity\EntityTest;
use Drupal\FunctionalJavascriptTests\WebDriverTestBase;
use Drupal\image\Plugin\Field\FieldType\ImageItem;
use Drupal\media\Entity\Media;
use Drupal\Tests\image_library_widget\Traits\ImageLibraryWidgetTestTrait;
use Drupal\Tests\media\Traits\MediaTypeCreationTrait;
use Drupal\Tests\TestFileCreationTrait;

/**
 * Tests the Image Library Widget.
 *
 * @group image_library_widget
 */
class ImageLibraryWidgetTest extends WebDriverTestBase {

  use ImageLibraryWidgetTestTrait;
  use MediaTypeCreationTrait;
  use TestFileCreationTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'entity_test',
    'image_library_widget',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    foreach (['logo' => 'Logo', 'banner' => 'Banner'] as $id => $label) {
      $this->createMediaType('image', [
        'id' => $id,
        'label' => $label,
        'source_configuration' => [
          'source_field' => 'image_library_widget_image',
        ],
      ])->save();
    }

    $this->createImageField([
      'type' => 'image_library_widget',
      'region' => 'content',
      'settings' => [
        'media_type_id' => 'logo',
      ],
    ]);

    $this->drupalLogin($this->createUser([
      'access content',
      'administer entity_test content',
      'view media',
    ]));
  }

  /**
   * Tests the Image Library Widget.
   */
  public function testWidget(): void {
    $this->drupalGet('/entity_test/add');

    $assert = $this->assertSession();
    $session = $this->getSession();
    $page = $session->getPage();

    // Check that when there are not media entries the library is not displayed.
    $assert->elementNotExists('css', '.image-library-widget');

    // Add some pre-uploaded logos.
    $this->addMedia('logo', 30);

    $session->reload();

    // Check that the label doesn't exist as it hasn't been configured.
    $assert->elementNotExists('css', '.image-library-widget-label');

    /** @var \Drupal\Core\Entity\Display\EntityFormDisplayInterface $form_display */
    $form_display = EntityFormDisplay::load('entity_test.entity_test.default');
    // Add image library browser label.
    $widget = $form_display->getComponent('image');
    $widget['settings']['library_label'] = 'Or choose a predefined Logo';
    $form_display->setComponent('image', $widget)->save();

    $session->reload();

    // Check that the image library is displayed.
    $assert->elementExists('css', '.image-library-widget-label');
    $assert->pageTextContains('Or choose a predefined Logo');
    $assert->elementExists('css', '.image-library-widget');

    $image_fid = $this->selectImageFromLibrary();

    // Check that after a image has been selected the browser won't show.
    $assert->elementNotExists('css', '.image-library-widget-label');
    $assert->pageTextNotContains('Or choose a predefined Logo');
    $assert->elementNotExists('css', '.image-library-widget');

    // Save the new entity.
    $page->pressButton('Save');

    // Check that the image has been attached to the new entity.
    $this->assertEquals($image_fid, EntityTest::load(1)->get('image')->target_id);

    // Remove the image.
    $page->pressButton('Remove');
    $assert->assertWaitOnAjaxRequest();

    // Test the pager.
    $assert->pageTextContainsOnce('Page 1');
    $assert->linkExists('Next ›');
    $assert->linkNotExists('‹ Previous');

    $this->clickLink('Next ›');
    $assert->assertWaitOnAjaxRequest();

    $assert->pageTextContainsOnce('Page 2');
    $assert->linkExists('Next ›');
    $assert->linkExists('‹ Previous');

    // Check that image selection works also in Page 2.
    $image_fid = $this->selectImageFromLibrary();
    $page->pressButton('Save');
    \Drupal::entityTypeManager()->getStorage('entity_test')->resetCache([1]);
    $this->assertEquals($image_fid, EntityTest::load(1)->get('image')->target_id);
  }

  /**
   * Selects a random image from the image library and returns its file ID.
   *
   * @return int
   *   The selected image file ID.
   *
   * @throws \Behat\Mink\Exception\ElementNotFoundException
   *   If either images from the library or the hidden field doesn't exist.
   * @throws \Behat\Mink\Exception\ExpectationException
   *   If the hidden field's value doesn't meet the expectation.
   */
  protected function selectImageFromLibrary(): int {
    // Get the image link node elements from the image library browser.
    $image_links = $this->cssSelect('.image-library-widget .views-field-image-library-widget-image a.image-library-widget-link');
    // Select a random image link from the library.
    $image_link = $image_links[array_rand($image_links)];

    $image_link->click();

    // Get the class the identifies the image file entity by their ID.
    $image_fid_class = array_values(array_filter(
      preg_split('/\s+/', $image_link->getAttribute('class')),
      function (string $class): bool {
        return (bool) preg_match('/^image\-\d+$/', $class);
      }
    ))[0];
    $image_fid = (int) explode('-', $image_fid_class, 2)[1];

    // After clicking the image, an Ajax request is executed. Wait to finish.
    $this->assertSession()->assertWaitOnAjaxRequest();

    // Check that the hidden file ID field has been updated by Javascript.
    $this->assertSession()->hiddenFieldValueEquals('image[0][fids]', $image_fid);

    return $image_fid;
  }

  /**
   * Creates a number of media entities.
   *
   * @param string $media_type_id
   *   The type of media to be created.
   * @param int $amount
   *   The amount of media entities to be created.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   *   In case of failures an exception is thrown.
   */
  protected function addMedia(string $media_type_id, int $amount): void {
    /** @var \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager */
    $entity_field_manager = \Drupal::service('entity_field.manager');
    $field_definition = $entity_field_manager->getFieldDefinitions('media', $media_type_id)['image_library_widget_image'];
    for ($i = 0; $i < $amount; $i++) {
      Media::create([
        'bundle' => $media_type_id,
        'name' => $this->randomString(),
        'image_library_widget_image' => [
          'target_id' => ImageItem::generateSampleValue($field_definition)['target_id'],
        ],
      ])->save();
    }
  }

}
