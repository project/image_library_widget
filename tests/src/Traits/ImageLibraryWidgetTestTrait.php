<?php

declare(strict_types = 1);

namespace Drupal\Tests\image_library_widget\Traits;

use Drupal\Core\Entity\Entity\EntityFormDisplay;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;

/**
 * Reusable code for tests.
 */
trait ImageLibraryWidgetTestTrait {

  /**
   * Creates an 'image' image field on the 'entity_test' module.
   *
   * @param array $widget_options
   *   (optional) The caller may provide the widget options.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   *   In case of failures an exception is thrown.
   */
  protected function createImageField(array $widget_options = []): void {
    FieldStorageConfig::create([
      'entity_type' => 'entity_test',
      'type' => 'image',
      'field_name' => 'image',
    ])->save();
    FieldConfig::create([
      'entity_type' => 'entity_test',
      'bundle' => 'entity_test',
      'field_name' => 'image',
      'label' => 'Image',
    ])->setSetting('alt_field', FALSE)
      ->save();

    /** @var \Drupal\Core\Entity\Display\EntityFormDisplayInterface $entity_form_display */
    $entity_form_display = EntityFormDisplay::load('entity_test.entity_test.default');

    if (!$entity_form_display) {
      $entity_form_display = EntityFormDisplay::create([
        'targetEntityType' => 'entity_test',
        'bundle' => 'entity_test',
        'mode' => 'default',
        'status' => TRUE,
      ]);
    }

    $entity_form_display->setComponent('image', $widget_options)->save();
  }

}
