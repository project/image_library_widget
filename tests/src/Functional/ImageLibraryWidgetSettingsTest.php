<?php

declare(strict_types = 1);

namespace Drupal\Tests\image_library_widget\Functional;

use Drupal\Tests\BrowserTestBase;
use Drupal\Tests\image_library_widget\Traits\ImageLibraryWidgetTestTrait;

/**
 * Tests the Image Library widget settings.
 *
 * @group image_library_widget
 */
class ImageLibraryWidgetSettingsTest extends BrowserTestBase {

  use ImageLibraryWidgetTestTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'entity_test',
    'field_ui',
    'image_library_widget',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->createImageField();
    $this->createRole(['administer entity_test form display'], 'testing_role');
    $this->drupalLogin($this->createUser([], NULL, FALSE, ['roles' => ['testing_role']]));
  }

  /**
   * Tests the Image Library widget settings.
   */
  public function testWidgetSettings(): void {
    $this->drupalGet('entity_test/structure/entity_test/form-display');

    $assert_session = $this->assertSession();
    $session = $this->getSession();
    $page = $session->getPage();

    // The widget is not configured.
    $assert_session->pageTextNotContains('Image library type:');

    // Use the 'Image Library Widget' widget.
    $this->submitForm([
      'fields[image][type]' => 'image_library_widget',
    ], 'Save');

    // Enter the widget settings form.
    $this->click('input[name="image_settings_edit"]');

    // Check that the warning message for non media type admins is displayed.
    $assert_session->pageTextContains('There are no media types configured to be used as image libraries. Ask the site administrator to add some media types, configured to act as image libraries. Until then the widget will act as normal image widget.');

    // Make the user able to administer media types.
    user_role_grant_permissions('testing_role', ['administer media types']);

    $session->reload();

    // Check that the warning message for media type admins is displayed.
    $assert_session->pageTextNotContains('There are no media types configured to be used as image libraries. Ask the site administrator to add some media types, configured to act as image libraries. Until then the widget will act as normal image widget.');
    $assert_session->pageTextContains('There are no media types configured to be used as image libraries. Until you add some media types, configured to act as image libraries, the widget will act as normal image widget.');
    $assert_session->linkExists('add some media types');

    // Add two media types to be used as image libraries for the widget.
    $this->clickLink('add some media types');
    $this->submitForm([
      'label' => 'Logo',
      'id' => 'logo',
      'source' => 'image',
    ], 'Save');
    $page->selectFieldOption('Field with source information', 'media.image_library_widget_image');
    $page->pressButton('Save');
    $this->drupalGet('admin/structure/media/add');

    $this->submitForm([
      'label' => 'Banner',
      'id' => 'banner',
      'source' => 'image',
    ], 'Save');
    $page->selectFieldOption('Field with source information', 'media.image_library_widget_image');
    $page->pressButton('Save');
    $this->drupalGet('admin/structure/media/add');

    // Add a media type that doesn't qualify as an image library for the widget.
    $this->submitForm([
      'label' => 'Not a widget image library',
      'id' => 'not_a_widget_image_library',
      'source' => 'file',
    ], 'Save');
    $page->pressButton('Save');

    // Check that the new media types were added.
    $assert_session->pageTextContains('Logo');
    $assert_session->pageTextContains('Banner');
    $assert_session->pageTextContains('Not a widget image library');

    // Enter the widget settings form.
    $this->drupalGet('entity_test/structure/entity_test/form-display');
    $this->click('input[name="image_settings_edit"]');

    // Check that only correctly configured media types can be selected.
    $assert_session->optionExists('Image library type', 'Logo');
    $assert_session->optionExists('Image library type', 'Banner');
    $assert_session->optionNotExists('Image library type', 'Not a widget image library');

    $page->selectFieldOption('Image library type', 'Logo');
    $page->fillField('A label to be displayed above the library browser', 'Or choose a predefined Logo');
    $page->pressButton('Update');
    $page->pressButton('Save');

    // The widget is now configured.
    $assert_session->pageTextContains('Image library type: Logo');
  }

}
