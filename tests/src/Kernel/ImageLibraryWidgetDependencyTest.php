<?php

declare(strict_types = 1);

namespace Drupal\Tests\image_library_widget\Kernel;

use Drupal\Core\Entity\Entity\EntityFormDisplay;
use Drupal\KernelTests\KernelTestBase;
use Drupal\media\Entity\MediaType;
use Drupal\Tests\image_library_widget\Traits\ImageLibraryWidgetTestTrait;

/**
 * Tests the widget dependency removal.
 *
 * @group image_library_widget
 */
class ImageLibraryWidgetDependencyTest extends KernelTestBase {

  use ImageLibraryWidgetTestTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'entity_test',
    'field',
    'file',
    'image',
    'image_library_widget',
    'media',
  ];

  /**
   * Tests the widget dependency removal.
   */
  public function testDependencyRemoval(): void {
    MediaType::create([
      'label' => 'Logo',
      'id' => 'logo',
      'source' => 'image',
      'source_configuration' => [
        'source_field' => 'image_library_widget_image',
      ],
    ])->save();

    $this->createImageField([
      'type' => 'image_library_widget',
      'settings' => [
        'media_type_id' => 'logo',
      ],
    ]);

    /** @var \Drupal\Core\Entity\Display\EntityFormDisplayInterface $entity_form_display */
    $entity_form_display = EntityFormDisplay::load('entity_test.entity_test.default');

    // Check that a dependency to 'logo' media type has been added.
    $this->assertContains('media.type.logo', $entity_form_display->getDependencies()['config']);

    // Remove the dependency.
    MediaType::load('logo')->delete();

    // Reload the form display.
    $entity_form_display = EntityFormDisplay::load('entity_test.entity_test.default');

    // Check that the 'image' widget has not been disabled.
    $this->assertNotNull($entity_form_display->getComponent('image'));

    // Check that the 'media_type_id' setting is NULL.
    $this->assertNull($entity_form_display->getComponent('image')['settings']['media_type_id']);
  }

}
