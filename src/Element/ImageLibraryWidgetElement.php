<?php

declare(strict_types = 1);

namespace Drupal\image_library_widget\Element;

use Drupal\file\Element\ManagedFile;

/**
 * Extends the core's managed file form element.
 *
 * Overrides the core form element just to get a custom theme variant as we want
 * to render the image library browser without the managed file element,
 * because if we do so, it would be rendered before the field's description. But
 * the field description is meaningful only for the managed file widget itself.
 * For this reason, we'll append the image library browser just after the main
 * field description, in the form_element__image_library_widget template.
 *
 * @see \image_library_widget_theme()
 * @see \image_library_widget_theme_suggestions_form_element()
 * @see \Drupal\image_library_widget\Plugin\Field\FieldWidget\ImageLibraryWidget::formElement()
 *
 * @FormElement("image_library_widget")
 */
class ImageLibraryWidgetElement extends ManagedFile {}
