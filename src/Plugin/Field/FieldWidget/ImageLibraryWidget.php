<?php

declare(strict_types = 1);

namespace Drupal\image_library_widget\Plugin\Field\FieldWidget;

use Drupal\Core\Block\BlockManagerInterface;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Config\Entity\ConfigEntityStorageInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Image\ImageFactory;
use Drupal\Core\Render\ElementInfoManagerInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\Url;
use Drupal\image\Plugin\Field\FieldWidget\ImageWidget;
use Drupal\media\MediaTypeInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides an image widget that allows uploading or choosing an image.
 *
 * @FieldWidget(
 *   id = "image_library_widget",
 *   label = @Translation("Image Library Widget"),
 *   field_types = {
 *     "image",
 *   },
 * )
 */
class ImageLibraryWidget extends ImageWidget {

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The block plugin manager service.
   *
   * @var \Drupal\Core\Block\BlockManagerInterface
   */
  protected $blockPluginManager;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * The media type entity storage.
   *
   * @var \Drupal\Core\Config\Entity\ConfigEntityStorageInterface
   */
  protected $mediaTypeStorage;

  /**
   * Constructs a new plugin instance.
   *
   * @param string $plugin_id
   *   The plugin_id for the widget.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The definition of the field to which the widget is associated.
   * @param array $settings
   *   The widget settings.
   * @param array $third_party_settings
   *   Any third party settings.
   * @param \Drupal\Core\Render\ElementInfoManagerInterface $element_info
   *   The element info manager service.
   * @param \Drupal\Core\Image\ImageFactory $image_factory
   *   The image factory service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   * @param \Drupal\Core\Block\BlockManagerInterface $block_plugin_manager
   *   The block plugin manager service.
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   The current user.
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, array $third_party_settings, ElementInfoManagerInterface $element_info, ImageFactory $image_factory, EntityTypeManagerInterface $entity_type_manager, BlockManagerInterface $block_plugin_manager, AccountProxyInterface $current_user) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $third_party_settings, $element_info, $image_factory);
    $this->entityTypeManager = $entity_type_manager;
    $this->blockPluginManager = $block_plugin_manager;
    $this->currentUser = $current_user;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): self {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['third_party_settings'],
      $container->get('element_info'),
      $container->get('image.factory'),
      $container->get('entity_type.manager'),
      $container->get('plugin.manager.block'),
      $container->get('current_user')
    );
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings(): array {
    return [
      'media_type_id' => NULL,
      'library_label' => NULL,
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state): array {
    $form = parent::settingsForm($form, $form_state);

    $media_type_ids = $this->getMediaTypeStorage()->getQuery()
      ->condition('source', 'image')
      ->condition('source_configuration.source_field', 'image_library_widget_image')
      ->execute();

    if (!$media_type_ids) {
      if ($this->currentUser->hasPermission('administer media types')) {
        $message = $this->t('There are no media types configured to be used as image libraries. Until you <a href=":url">add some media types</a>, configured to act as image libraries, the widget will act as normal image widget.', [
          ':url' => Url::fromRoute('entity.media_type.add_form')->toString(),
        ]);
      }
      else {
        $message = $this->t('There are no media types configured to be used as image libraries. Ask the site administrator to add some media types, configured to act as image libraries. Until then the widget will act as normal image widget.');
      }
      $form['warning'] = [
        '#theme' => 'status_messages',
        '#message_list' => ['warning' => [$message]],
      ];
      $form['media_type_id'] = [
        '#type' => 'value',
        '#value' => NULL,
      ];
    }
    else {
      $options = [];
      foreach ($this->getMediaTypeStorage()->loadMultiple($media_type_ids) as $media_type_id => $media_type) {
        $options[$media_type_id] = $media_type->label();
      }

      $form['media_type_id'] = [
        '#type' => 'select',
        '#title' => $this->t('Image library type'),
        '#options' => $options,
        '#default_value' => $this->getSetting('media_type_id'),
        '#required' => TRUE,
      ];
      $form['library_label'] = [
        '#type' => 'textfield',
        '#title' => $this->t('A label to be displayed above the library browser'),
        '#description' => $this->t('Use the @type placeholder to indicate the type of library.'),
        '#default_value' => $this->getSetting('library_label'),
      ];
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary(): array {
    $summary = parent::settingsSummary();

    if ($this->fieldDefinition->getFieldStorageDefinition()->getCardinality() !== 1) {
      $summary[] = $this->t("<strong>Warning!</strong> %label (<code>@name</code>) is a multi-value field. The <em>Image Library Widget</em> doesn't support yet fields with a cardinality other than 1. Support multi-value image fields to be added in <a href='https://www.drupal.org/project/image_library_widget/issues/3103574'>#3103574</a>.", [
        '%label' => $this->fieldDefinition->getLabel(),
        '@name' => $this->fieldDefinition->getName(),
      ]);
    }
    elseif ($this->getSetting('media_type_id')) {
      $summary[] = $this->t('Image library type: @media_type', [
        '@media_type' => $this->getMediaType()->label(),
      ]);
    }

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state): array {
    $element = parent::formElement($items, $delta, $element, $form, $form_state);

    $field_name = $element['#field_name'];
    $cardinality = $this->fieldDefinition->getFieldStorageDefinition()->getCardinality();

    // Only image fields with a single value (0..1) are currently supported.
    // @todo Support multi-value image fields in #3103574.
    // @see https://www.drupal.org/project/image_library_widget/issues/3103574
    if ($cardinality !== 1) {
      throw new \Exception("Image Library Widget multi-value fields is not supported yet.");
    }

    // Only show when we're on the item that renders the upload button.
    if (!$items[$delta]->isEmpty() || !empty($form_state->getValue($field_name)[0])) {
      return $element;
    }

    // We don't add the image library browser as a child of the element because
    // if we do so, it would be rendered before the field's description. But the
    // field description is meaningful only for the managed file widget itself.
    // Instead, we'll append it in the form_element__image_library_widget theme,
    // just after the description.
    // @see templates/form-element--image-library-widget.html.twig
    $element['#image_library_widget'] = [];

    $cache_metadata = new CacheableMetadata();

    // Vary on permissions as some users might not be allowed to access the
    // image library widget browser.
    $cache_metadata->addCacheContexts(['user.permissions']);

    /** @var \Drupal\Core\Entity\Display\EntityFormDisplayInterface $form_display */
    $form_display = $form_state->getStorage()['form_display'];
    // Invalidate the cache when the widget settings are updated.
    $cache_metadata->addCacheableDependency($form_display);

    if ($media_type_id = $this->getSetting('media_type_id')) {
      // @todo Use a configurable view and display in #3104544.
      // @see https://www.drupal.org/project/image_library_widget/issues/3104544
      /** @var \Drupal\views\Plugin\Block\ViewsBlock $block_plugin */
      $block_plugin = $this->blockPluginManager->createInstance("views_block:image_library_widget-block");
      if ($block_plugin->access($this->currentUser)) {
        $block_plugin->setContextValue('bundle', $media_type_id);
        $block_build = $block_plugin->build();

        // If the view display has been configured with a pager, it should use
        // Ajax, as navigating to a different widget page should be done without
        // reloading the page where the widget is rendered.
        /** @var \Drupal\views\ViewExecutable $view */
        if ($view =& $block_build['#view']) {
          if (!in_array($view->getPager()->getPluginId(), ['none', 'some']) && !$view->ajaxEnabled()) {
            throw new \Exception('When the image library widget is configured to use a pager, the view should be Ajax enabled.');
          }
        }

        // Prepare the browser label.
        $label = trim((string) $this->getSetting('library_label'));
        $label = $label && isset($view) ? str_replace('@type', $this->getMediaType()->label(), $label) : NULL;

        $element['#image_library_widget'] = [
          '#theme' => 'image_library_widget_browser',
          '#label' => $label,
          '#browser' => $block_build,
          '#has_results' => isset($view),
          '#attributes' => [
            'data-drupal-field' => $field_name,
            'class' => [
              'image-library-widget',
              'js-image-library-widget',
            ],
          ],
          '#attached' => [
            'library' => [
              'image_library_widget/browser',
            ],
          ],
        ];
      }
    }

    $cache_metadata->applyTo($element['#image_library_widget']);

    // Use a custom form render element just to be able to apply our own theme
    // template as we don't want to render the image library browser together
    // with the managed file element but we have to place it after the form
    // element.
    $element['#type'] = 'image_library_widget';

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function calculateDependencies(): array {
    $dependencies = parent::calculateDependencies();
    if ($media_type_id = $this->getSetting('media_type_id')) {
      $media_type = $this->getMediaTypeStorage()->load($media_type_id);
      // The widget depends on the media type ID.
      $dependencies[$media_type->getConfigDependencyKey()][] = $media_type->getConfigDependencyName();
    }
    return $dependencies;
  }

  /**
   * {@inheritdoc}
   */
  public function onDependencyRemoval(array $dependencies): bool {
    $changed = parent::onDependencyRemoval($dependencies);
    $media_type_id = $this->getSetting('media_type_id');
    if ($media_type_id && $media_type = $this->getMediaTypeStorage()->load($media_type_id)) {
      if (!empty($dependencies[$media_type->getConfigDependencyKey()][$media_type->getConfigDependencyName()])) {
        // Don't hide the widget if the corresponding media type is deleted.
        $this->setSetting('media_type_id', NULL);
        $changed = TRUE;
      }
    }
    return $changed;
  }

  /**
   * Returns the media type config entity for this widget.
   *
   * @return \Drupal\media\MediaTypeInterface|null
   *   The media type of this widget.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   *   Thrown if the 'media_type' storage handler couldn't be loaded.
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   *   Thrown if the 'media_type' entity type doesn't exist.
   */
  protected function getMediaType(): ?MediaTypeInterface {
    if ($media_type_id = $this->getSetting('media_type_id')) {
      /** @var \Drupal\media\MediaTypeInterface $media_type */
      $media_type = $this->getMediaTypeStorage()->load($media_type_id);
      return $media_type;
    }
    return NULL;
  }

  /**
   * Returns the media type entity storage.
   *
   * @return \Drupal\Core\Config\Entity\ConfigEntityStorageInterface
   *   The media type entity storage.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   *   Thrown if the 'media' storage handler couldn't be loaded.
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   *   Thrown if the 'media' entity type doesn't exist.
   */
  protected function getMediaTypeStorage(): ConfigEntityStorageInterface {
    if (!isset($this->mediaTypeStorage)) {
      $this->mediaTypeStorage = $this->entityTypeManager->getStorage('media_type');
    }
    return $this->mediaTypeStorage;
  }

}
