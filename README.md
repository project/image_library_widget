# Image Library

Provides an image widget allowing users to upload or choose an existing image
from a library with a predefined set of images. Each widget can be configured to
show images within a media type.

## How it works?

The widget extends the core's image widget but adds also an "image browser". The
"image browser" is actually a view that exposes image entries from the media
type that was configured with that widget. The user can use the standard upload
feature, or they can select a pre-uploaded image by clicking in the "image
browser".

## Dependencies

- Media (core)
- Views (core)

## Limitations

- Currently, the module supports only image fields with a single value
  (cardinality 1). Support multi-value image fields to be implemented in
  https://www.drupal.org/project/image_library_widget/issues/3103574.
- It's not impossible to configure the "image browser" (the view) with a pager
  but without Ajax being enabled, as navigating to a different widget page,
  should be done without reloading the page where the widget is rendered.

## Configuration

- Create one or more new media types by visiting `/admin/structure/media/add`.
  Set the "Media type" to "Image" and under "Media source configuration" set the
  "Field with source information" to "media.image_library_widget_image". Only
  media types configured in this way can be used as libraries in the widget.
- Give the appropriate permissions to site admins, so they can create media
  entries within those media types.
- Create some media entries.
- Configure the image field to use "Image Library Widget" as widget.
- In the widget settings form chose the media type to be used as image library.

## Customization

Because the "image browser" is a view, you can do whatever Views customization
you want. You can decide what display format to use (e.g. Grid, HTML List,
Unformatted list, etc.), or the size of image thumbnails. If there are too many
images pre-ulploaded in that media type, you can configure a pager but in this
case you'll need to enable Ajax (see "Limitations").
