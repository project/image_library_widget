/**
 * @file
 * Provides javascript functionality for image library widget.
 */

(function (Drupal) {
  /**
   * Allows to select a pre-uploaded image using an image library browser.
   *
   * @type {Drupal~behavior}
   *
   * @prop {Drupal~behaviorAttach} attach
   *   Attaches site-wide featured behaviors.
   */
  Drupal.behaviors.imageLibraryWidget = {
    attach: function () {
      const elements = once(
        "imageLibraryWidget",
        ".js-image-library-widget-view a.js-image-library-widget-link"
      );
      elements.forEach((element) => {
        element.addEventListener("click", (event) => {
          // Inhibit the default behavior when clicking the link.
          event.preventDefault();

          // Retrieve the image file ID.
          // @todo Get the image file ID from a data attribute in #3104544.
          // @see https://www.drupal.org/project/image_library_widget/issues/3104544
          let imageFileId = null;
          element.classList.forEach((className) => {
            let found = className.match(/image-(\d+)/);
            if (found !== null) {
              imageFileId = found[1];
            }
          });
          if (imageFileId === null) {
            throw new Error(
              "The image wrapper is missing a proper 'image-{FID}' class"
            );
          }

          // Get the sub-widget wrapper.
          const imageLibraryBrowser = element.closest(
            ".js-image-library-widget"
          );
          // Retrieve the field name.
          const fieldName = imageLibraryBrowser.dataset.drupalField;
          // Get the image widget element.
          const widgetElement = [
            ...imageLibraryBrowser.parentNode.children,
          ].filter((child) => {
            return (
              child !== imageLibraryBrowser &&
              child.classList.contains("image-widget")
            );
          })[0];

          // Using 0 delta as we're only supporting single value image fields.
          // @todo Support multi-value image fields in #3103574.
          // @see https://www.drupal.org/project/image_library_widget/issues/3103574
          const delta = 0;

          // Set the pre-uploaded image ID.
          const hiddenInput = widgetElement.querySelector(
            `input[type="hidden"][name="${fieldName}[${delta}][fids]"]`
          );
          hiddenInput.setAttribute("value", imageFileId);

          // Trigger the upload button to refresh the image preview.
          const uploadButton = widgetElement.querySelector(
            `input[type="submit"][name="${fieldName}_${delta}_upload_button"]`
          );
          uploadButton.dispatchEvent(new Event("mousedown"));
        });
      });
    },
  };
})(Drupal);
